#!/usr/bin/python

import MySQLdb
import pymongo
from pymongo import MongoClient

def main():
    # MySQL
    db = MySQLdb.connect(host='localhost',
            user='ip_log',
            passwd='ips+test',
            db='ip_log')
    cur = db.cursor()

    # mongo
    client = MongoClient()
    db = client.ip_log
    ips = db.ips        

    cur.execute("SELECT INET_NTOA(ips.ip) as ip, ips.ts from ips")

    for row in cur.fetchall():
        ip = { 'ip':row[0],
               'ts':row[1]}
        ips.insert(ip)


if __name__ == "__main__":
    main()
