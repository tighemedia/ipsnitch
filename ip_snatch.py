#!/usr/bin/python

import tornado.ioloop
import tornado.web

import MySQLdb 
import socket

PIS = {
        'front':{
            'ip':'192.168.1.167',
            'port':8084
        },
        'back':{
            'ip':'192.168.1.162',
            'port':8888
        }
    }

class DB:
    def connect(self):
        return MySQLdb.connect(user='root',passwd="reR3leases",db="ip_log")

class RestrictHandler(tornado.web.RequestHandler):
    def initialize(self):
        if not(self.is_valid_client(self.request.remote_ip)):
            self.set_status(403)
            self._transforms = []
            self.finish('Restricted')
    def is_valid_client(self, ip):
        q = socket.gethostbyaddr(ip)
        if 'rcmdva.fios.verizon.net' in q[0]:
            return True

class BoilerHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.db = DB().connect()
        self.cursor = self.db.cursor()
    def get_last_ip(self):
        try:
            self.cursor.execute('select INET_NTOA(ip) as ip, ts from ips order by ts DESC limit 1');
            row = self.cursor.fetchone()
            return row[0]
        except Exception, e:
            print e 
            pass
    def is_local(self):
        if self.request.remote_ip == self.get_last_ip():
            return True
        return False

class GetIpHandler(BoilerHandler):
    def get(self):
        self.write(self.get_last_ip()) 

class FrontHandler(BoilerHandler):
    def prepare(self):
        if self.request.method == "GET":
            if self.is_local():
                feed_url = 'http://%s:%s' % (PIS['front']['ip'], PIS['front']['port'])
            else:
                last_ip = self.get_last_ip()
                feed_url = 'http://%s:%s' % (last_ip, PIS['front']['port'])
            self.redirect(feed_url)

class BackHandler(BoilerHandler):
    def prepare(self):
        if self.request.method == "GET":
            if self.is_local():
                feed_url = 'http://%s:%s' % (PIS['back']['ip'], PIS['back']['port'])
            else:
                last_ip = self.get_last_ip()
                feed_url = 'http://%s:%s' % (last_ip, PIS['back']['port'])
            self.redirect(feed_url)

class MainHandler(BoilerHandler):
    def prepare(self):
        if self.request.method == "GET":
            last_ip = self.get_last_ip()
            #self.redirect()
            print last_ip
            feed_url = 'http://%s:8084' % (last_ip)
            print feed_url
            self.redirect(feed_url)

    def get(self):
        from tornado.escape import json_encode
        last_ip = self.get_last_ip()
        return self.write(last_ip+"\n")
   
    def post(self):
        last_ip = self.get_last_ip()
        if len(last_ip)>0:
            if self.request.remote_ip != last_ip:
                self.add_new_ip(self.request.remote_ip)
    
    def add_new_ip(self, ip):
        print 'new ip'
        try:
            self.cursor.execute("insert into ips (ip) values (INET_ATON('%s'))" % ip)
            self.db.commit()
        except Exception, e:
            print e

class AdminHandler(BoilerHandler):
    def get(self):
        self.render('static/index.html')

application = tornado.web.Application([
    (r"/", MainHandler),
    (r"/admin", AdminHandler),
    (r"/front", FrontHandler),
    (r"/back", BackHandler),
    (r"/ip", GetIpHandler),
    (r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "/var/www/ip_snatch/static/"}),
], debug=True)

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
